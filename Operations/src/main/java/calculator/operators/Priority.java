package calculator.operators;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public enum Priority {
    LOW,
    MIDDLE,
    HIGH,
    HIGHEST
}
