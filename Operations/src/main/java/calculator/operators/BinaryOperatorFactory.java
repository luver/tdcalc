package calculator.operators;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public interface BinaryOperatorFactory {
    BinaryOperator get();
}
