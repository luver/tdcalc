package calculator.operators;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public enum Associativity {
    LEFT,
    RIGHT
}
