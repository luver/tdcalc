package calculator.operators.standardOperators;

import calculator.operators.BinaryOperator;
import calculator.operators.BinaryOperatorFactory;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class MinusOperatorFactory implements BinaryOperatorFactory {
    private final BinaryOperator operator = new MinusOperator();

    @Override
    public BinaryOperator get() {
        return operator;
    }
}
