package calculator.operators.standardOperators;

import calculator.operators.Associativity;
import calculator.operators.BinaryOperator;
import calculator.operators.Priority;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class PowerOperator extends BinaryOperator {
    final private Priority priority = Priority.HIGH;
    final private Associativity associativity = Associativity.RIGHT;

    @Override
    public double exec(double leftArgument, double rightArgument) {
        return Math.pow(leftArgument, rightArgument);
    }

    @Override
    public Priority getPriority() {
        return priority;
    }


    @Override
    public Associativity getAssociativity() {
        return associativity;
    }
}
