package calculator.operators.standardOperators;

import calculator.operators.BinaryOperator;
import calculator.operators.BinaryOperatorFactory;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class PlusOperatorFactory implements BinaryOperatorFactory {
    private final BinaryOperator operator = new PlusOperator();

    @Override
    public BinaryOperator get() {
        return operator;
    }
}
