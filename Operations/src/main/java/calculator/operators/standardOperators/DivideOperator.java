package calculator.operators.standardOperators;

import calculator.operators.Associativity;
import calculator.operators.BinaryOperator;
import calculator.operators.Priority;

import static calculator.operators.Associativity.LEFT;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class DivideOperator extends BinaryOperator {
    final private Priority priority = Priority.MIDDLE;
    final private Associativity associativity = LEFT;

    @Override
    public double exec(double leftArgument, double rightArgument) {
        double result = leftArgument / rightArgument;
        if (result == Double.POSITIVE_INFINITY) {
            throw new IllegalArgumentException("Dividing by zero");
        }
        return result;
    }

    @Override
    public Priority getPriority() {
        return priority;
    }

    @Override
    public Associativity getAssociativity() {
        return associativity;
    }
}
