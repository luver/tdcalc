package calculator.operators.standardOperators;

import calculator.operators.Associativity;
import calculator.operators.BinaryOperator;
import calculator.operators.Priority;

import static calculator.operators.Associativity.LEFT;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class PlusOperator extends BinaryOperator {
    final private Priority priority = Priority.LOW;
    final private Associativity associativity = LEFT;

    @Override
    public double exec(double leftArgument, double rightArgument) {
        return leftArgument + rightArgument;
    }


    @Override
    public Priority getPriority() {
        return priority;
    }


    @Override
    public Associativity getAssociativity() {
        return associativity;
    }
}
