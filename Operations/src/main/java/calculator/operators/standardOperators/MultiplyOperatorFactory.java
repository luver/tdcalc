package calculator.operators.standardOperators;

import calculator.operators.BinaryOperator;
import calculator.operators.BinaryOperatorFactory;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class MultiplyOperatorFactory implements BinaryOperatorFactory {
    private final BinaryOperator operator = new MultiplyOperator();

    @Override
    public BinaryOperator get() {
        return operator;
    }
}
