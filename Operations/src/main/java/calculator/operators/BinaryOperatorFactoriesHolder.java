package calculator.operators;

import java.util.HashMap;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class BinaryOperatorFactoriesHolder {
    HashMap<String, BinaryOperatorFactory> functionFactories = new HashMap<String, BinaryOperatorFactory>();

    public void add(String name, BinaryOperatorFactory factory) {
        functionFactories.put(name, factory);
    }

    public BinaryOperatorFactory getFactory(String name) {
        return functionFactories.get(name);
    }
}
