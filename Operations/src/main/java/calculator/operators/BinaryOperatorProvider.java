package calculator.operators;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class BinaryOperatorProvider {
    private BinaryOperatorFactoriesHolder holder;

    public BinaryOperatorProvider(BinaryOperatorFactoriesHolder holder) {
        this.holder = holder;
    }

    public BinaryOperator get(String name) {
        BinaryOperatorFactory factory = holder.getFactory(name);
        if (factory != null) {
            return factory.get();
        }
        return null;
    }
}
