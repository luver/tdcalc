package calculator.operators;

import static calculator.operators.Associativity.LEFT;
import static calculator.operators.Associativity.RIGHT;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
abstract public class BinaryOperator {
    abstract public double exec(double leftArgument, double rightArgument);

    abstract public Priority getPriority();

    abstract public Associativity getAssociativity();

    public int compareTo(BinaryOperator operator) {
        if (this.getAssociativity() == LEFT) {
            if (this.getPriority().compareTo(operator.getPriority()) <= 0)
                return -1;
        }
        if (this.getAssociativity() == RIGHT) {
            if (this.getPriority().compareTo(operator.getPriority()) < 0) {
                return -1;
            }
        }
        return 1;
    }
}
