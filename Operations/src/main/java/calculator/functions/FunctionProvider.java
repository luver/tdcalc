package calculator.functions;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class FunctionProvider {
    private FunctionFactoriesHolder holder;

    public FunctionProvider(FunctionFactoriesHolder holder) {
        this.holder = holder;
    }

    public Function get(String name) {
        FunctionFactory factory = holder.getFactory(name);
        if (factory != null) {
            return factory.get();
        }
        return null;
    }
}
