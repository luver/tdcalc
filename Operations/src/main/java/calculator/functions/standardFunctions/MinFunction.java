package calculator.functions.standardFunctions;

import calculator.functions.Function;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class MinFunction implements Function {
    @Override
    public double exec(double... arguments) {
        if (arguments.length == 0) throw new RuntimeException("Bad arguments for min()");
        double min = arguments[0];
        for (double element : arguments) {
            min = (element < min) ? element : min;
        }
        return min;
    }
}
