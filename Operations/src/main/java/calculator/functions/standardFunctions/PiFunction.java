package calculator.functions.standardFunctions;

import calculator.functions.Function;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class PiFunction implements Function {
    @Override
    public double exec(double... arguments) {
        if (arguments.length > 0) throw new RuntimeException("Bad arguments for pi()");
        return 3.1415;
    }
}
