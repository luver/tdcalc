package calculator.functions.standardFunctions;

import calculator.functions.Function;
import calculator.functions.FunctionFactory;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class MinFunctionFactory implements FunctionFactory {
    private final Function function = new MinFunction();

    @Override
    public Function get() {
        return function;
    }
}
