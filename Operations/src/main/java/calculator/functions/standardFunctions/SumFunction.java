package calculator.functions.standardFunctions;

import calculator.functions.Function;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class SumFunction implements Function {
    @Override
    public double exec(double... arguments) {
        if (arguments.length == 0) throw new RuntimeException("Bad arguments for sum()");
        double sum = 0;
        for (double argument : arguments) {
            sum += argument;
        }
        return sum;
    }
}
