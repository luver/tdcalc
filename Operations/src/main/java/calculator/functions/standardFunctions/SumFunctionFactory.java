package calculator.functions.standardFunctions;

import calculator.functions.Function;
import calculator.functions.FunctionFactory;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class SumFunctionFactory implements FunctionFactory {
    private final Function function = new SumFunction();

    @Override
    public Function get() {
        return function;
    }
}
