package calculator.functions.standardFunctions;

import calculator.functions.Function;
import calculator.functions.FunctionFactory;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class PiFunctionFactory implements FunctionFactory {
    private final Function function = new PiFunction();

    @Override
    public Function get() {
        return function;
    }
}
