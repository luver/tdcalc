package calculator.functions.standardFunctions;

import calculator.functions.Function;
import calculator.functions.FunctionFactory;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class MaxFunctionFactory implements FunctionFactory {
    private final Function function = new MaxFunction();

    @Override
    public Function get() {
        return function;
    }
}
