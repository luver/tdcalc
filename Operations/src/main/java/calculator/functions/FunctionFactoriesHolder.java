package calculator.functions;

import java.util.HashMap;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class FunctionFactoriesHolder {
    HashMap<String, FunctionFactory> functionFactories = new HashMap<String, FunctionFactory>();

    public void add(String name, FunctionFactory factory) {
        functionFactories.put(name, factory);
    }

    public FunctionFactory getFactory(String name) {
        return functionFactories.get(name);
    }
}
