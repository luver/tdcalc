package calculator.functions;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public interface Function {
    double exec(double... arguments);
}
