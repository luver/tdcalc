package calculator.functions;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public interface FunctionFactory {
    Function get();
}
