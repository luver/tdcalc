package calculator.api;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public interface Solver {
    double solve(String expression) throws SolvingException;
}
