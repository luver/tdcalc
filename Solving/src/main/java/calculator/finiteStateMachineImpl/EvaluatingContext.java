package calculator.finiteStateMachineImpl;

import calculator.functions.Function;
import calculator.operators.BinaryOperator;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class EvaluatingContext {
    public Deque<Double> getArgumentsStack() {
        return argumentsStack;
    }

    public Deque<BinaryOperator> getOperatorsStack() {
        return operatorsStack;
    }

    private Deque<Double> argumentsStack = new ArrayDeque<Double>();
    private Deque<BinaryOperator> operatorsStack = new ArrayDeque<BinaryOperator>();

    public Deque<Integer> getArgumentsCountStack() {
        return argumentsCountStack;
    }

    public Deque<Integer> getBracketStack() {
        return bracketStack;
    }

    private Deque<Integer> bracketStack = new ArrayDeque<Integer>();
    private Deque<Integer> argumentsCountStack = new ArrayDeque<Integer>();
    private Deque<Integer> functionPositionStack = new ArrayDeque<Integer>();
    private Deque<Function> functionsStack = new ArrayDeque<Function>();

    public Deque<Integer> getFunctionPositionStack() {
        return functionPositionStack;
    }

    public Deque<Function> getFunctionsStack() {
        return functionsStack;
    }
}
