package calculator.finiteStateMachineImpl;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class InterpretingContext {

    public InterpretingContext(String expression) {
        parsingContext = new ParsingContext(expression);
        evaluatingContext = new EvaluatingContext();
    }

    public ParsingContext getParsingContext() {
        return parsingContext;
    }

    public EvaluatingContext getEvaluatingContext() {
        return evaluatingContext;
    }

    private ParsingContext parsingContext;
    private EvaluatingContext evaluatingContext;
}
