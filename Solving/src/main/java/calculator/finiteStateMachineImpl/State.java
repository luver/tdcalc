package calculator.finiteStateMachineImpl;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public enum State {
    START,
    NUMBER,
    OPEN_BRACKET,
    CLOSE_BRACKET,
    OPERATOR,
    ARGUMENTS_DELIMITER,
    FUNCTION,
    FINISH
}
