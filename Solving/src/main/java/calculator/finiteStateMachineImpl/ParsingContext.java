package calculator.finiteStateMachineImpl;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class ParsingContext {
    final private String expression;
    private int cursor;
    private boolean isParsed;

    public ParsingContext(String expression) {
        this.expression = expression;
    }


    public String getExpression() {
        return expression;
    }

    public boolean isParsed() {
        return isParsed;
    }

    public int getCursor() {
        return cursor;
    }

    public void setCursor(int cursor) {
        if (cursor >= expression.length()) isParsed = true;
        this.cursor = cursor;
    }

    public void incCursor() {
        cursor++;
    }

    public char getCurrentChar() {
        return expression.charAt(cursor);
    }
}
