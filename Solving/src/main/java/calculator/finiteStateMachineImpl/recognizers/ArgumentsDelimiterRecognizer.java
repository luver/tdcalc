package calculator.finiteStateMachineImpl.recognizers;

import calculator.finiteStateMachineImpl.ParsingContext;
import calculator.finiteStateMachineImpl.recognizers.commands.ArgumentsDelimiterCommand;
import calculator.finiteStateMachineImpl.recognizers.commands.InterpretingCommand;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class ArgumentsDelimiterRecognizer implements Recognizer {
    final private char delimiterSymbol = ',';

    @Override
    public InterpretingCommand getCommand(ParsingContext context) {
        if (parse(context)) {
            return new ArgumentsDelimiterCommand();
        }
        return null;
    }

    private boolean parse(ParsingContext context) {
        String expression = context.getExpression();
        while (!context.isParsed() && expression.charAt(context.getCursor()) == ' ') {
            context.setCursor(context.getCursor() + 1);
        }
        if (!context.isParsed()) {
            if (expression.charAt(context.getCursor()) == delimiterSymbol) {
                context.setCursor(context.getCursor() + 1);
                return true;
            }
        }
        return false;
    }
}
