package calculator.finiteStateMachineImpl.recognizers;

import calculator.finiteStateMachineImpl.ParsingContext;
import calculator.finiteStateMachineImpl.recognizers.commands.FunctionCommand;
import calculator.finiteStateMachineImpl.recognizers.commands.InterpretingCommand;
import calculator.functions.Function;
import calculator.functions.FunctionFactoriesHolder;
import calculator.functions.FunctionProvider;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class FunctionRecognizer implements Recognizer {
    private FunctionProvider functionProvider;

    public FunctionRecognizer(FunctionFactoriesHolder holder) {
        this.functionProvider = new FunctionProvider(holder);
    }

    @Override
    public InterpretingCommand getCommand(ParsingContext context) {
        Function function = getFunction(context);
        if (function != null) {
            return new FunctionCommand(function);
        }
        return null;
    }

    private Function getFunction(ParsingContext context) {
        String functionName = "";
        if (Character.isLetter(context.getCurrentChar())) {
            functionName = functionName + context.getCurrentChar();
            context.incCursor();
            while (!context.isParsed()) {
                if (acceptChar(context.getCurrentChar())) {
                    functionName = functionName + context.getCurrentChar();
                    context.incCursor();
                } else {
                    break;
                }
            }
        }
        return functionProvider.get(functionName);
    }

    private boolean acceptChar(char character) {
        return Character.isLetter(character) || Character.isDigit(character);
    }
}
