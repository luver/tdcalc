package calculator.finiteStateMachineImpl.recognizers;

import calculator.finiteStateMachineImpl.State;
import calculator.functions.FunctionFactoriesHolder;
import calculator.operators.BinaryOperatorFactoriesHolder;

import java.util.HashMap;
import java.util.Map;

import static calculator.finiteStateMachineImpl.State.*;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class RecognizerFactory {
    public RecognizerFactory(final FunctionFactoriesHolder functionFactoriesHolder,
                             final BinaryOperatorFactoriesHolder operatorFactoriesHolder) {
        recognizers = new HashMap<State, Recognizer>() {
            {
                put(NUMBER, new NumberRecognizer());
                put(OPERATOR, new OperatorRecognizer(operatorFactoriesHolder));
                put(FINISH, new FinishRecognizer());
                put(OPEN_BRACKET, new OpenBracketRecognizer());
                put(CLOSE_BRACKET, new CloseBracketRecognizer());
                put(ARGUMENTS_DELIMITER, new ArgumentsDelimiterRecognizer());
                put(FUNCTION, new FunctionRecognizer(functionFactoriesHolder));
            }
        };
    }

    Map<State, Recognizer> recognizers;

    public Recognizer get(State state) {
        return recognizers.get(state);
    }
}
