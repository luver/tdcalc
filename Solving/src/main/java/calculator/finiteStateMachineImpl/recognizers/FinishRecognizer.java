package calculator.finiteStateMachineImpl.recognizers;

import calculator.finiteStateMachineImpl.ParsingContext;
import calculator.finiteStateMachineImpl.recognizers.commands.FinishCommand;
import calculator.finiteStateMachineImpl.recognizers.commands.InterpretingCommand;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class FinishRecognizer implements Recognizer {
    @Override
    public InterpretingCommand getCommand(ParsingContext context) {
        if (context.isParsed()) {
            return new FinishCommand();
        }
        return null;
    }

}
