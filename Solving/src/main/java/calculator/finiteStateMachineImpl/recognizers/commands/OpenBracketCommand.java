package calculator.finiteStateMachineImpl.recognizers.commands;

import calculator.finiteStateMachineImpl.EvaluatingContext;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class OpenBracketCommand implements InterpretingCommand {
    @Override
    public void execute(EvaluatingContext context) {
        context.getBracketStack().push(context.getOperatorsStack().size());
    }
}
