package calculator.finiteStateMachineImpl.recognizers.commands;

import calculator.finiteStateMachineImpl.EvaluatingContext;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class CloseBracketCommand implements InterpretingCommand {

    public void execute(EvaluatingContext context) {
        int previousSize = context.getBracketStack().pop();

        while (context.getOperatorsStack().size() > previousSize) {
            double rightParam = context.getArgumentsStack().pop();
            double leftParam = context.getArgumentsStack().pop();
            double result = context.getOperatorsStack().pop().exec(leftParam, rightParam);
            context.getArgumentsStack().push(result);
        }

        // If there is some function
        if (!context.getFunctionPositionStack().isEmpty()) {
            if (context.getFunctionPositionStack().getFirst() == previousSize) {
                int argumentsCount = context.getArgumentsStack().size() - context.getArgumentsCountStack().pop();
                double[] arguments = new double[argumentsCount];
                for (int i = argumentsCount - 1; i >= 0; i--) {
                    arguments[i] = context.getArgumentsStack().pop();
                }
                context.getArgumentsStack().push(context.getFunctionsStack().pop().exec(arguments));
                context.getFunctionPositionStack().pop();
            }
        }
    }
}
