package calculator.finiteStateMachineImpl.recognizers.commands;

import calculator.finiteStateMachineImpl.EvaluatingContext;
import calculator.operators.BinaryOperator;

import java.util.Deque;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class OperatorCommand implements InterpretingCommand {
    private final BinaryOperator operator;

    public OperatorCommand(BinaryOperator operator) {
        this.operator = operator;
    }

    @Override
    public void execute(EvaluatingContext context) {
        Deque<BinaryOperator> operatorsStack = context.getOperatorsStack();
        int shift = (context.getBracketStack().isEmpty()) ? 0 : context.getBracketStack().peek();

        while (operatorsStack.size() - shift > 0) {
            if (operator.compareTo(operatorsStack.peek()) < 0) {
                double rightParam = context.getArgumentsStack().pop();
                double leftParam = context.getArgumentsStack().pop();
                double result = operatorsStack.pop().exec(leftParam, rightParam);
                context.getArgumentsStack().push(result);
            } else break;
        }
        operatorsStack.push(operator);
    }
}
