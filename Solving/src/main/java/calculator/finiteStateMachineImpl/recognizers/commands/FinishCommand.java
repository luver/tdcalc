package calculator.finiteStateMachineImpl.recognizers.commands;

import calculator.finiteStateMachineImpl.EvaluatingContext;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class FinishCommand implements InterpretingCommand {
    @Override
    public void execute(EvaluatingContext context) {
        while (!context.getOperatorsStack().isEmpty()) {
            double rightParam = context.getArgumentsStack().pop();
            double leftParam = context.getArgumentsStack().pop();
            double result = context.getOperatorsStack().pop().exec(leftParam, rightParam);
            context.getArgumentsStack().push(result);
        }
    }
}
