package calculator.finiteStateMachineImpl.recognizers.commands;

import calculator.finiteStateMachineImpl.EvaluatingContext;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class NumberCommand implements InterpretingCommand {
    private final double number;

    public NumberCommand(double number) {
        this.number = number;
    }


    @Override
    public void execute(EvaluatingContext context) {
        context.getArgumentsStack().push(number);
    }
}
