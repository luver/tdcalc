package calculator.finiteStateMachineImpl.recognizers.commands;

import calculator.finiteStateMachineImpl.EvaluatingContext;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class ArgumentsDelimiterCommand implements InterpretingCommand {
    @Override
    public void execute(EvaluatingContext context) {
        int previousSize = context.getBracketStack().getFirst();
        while (context.getOperatorsStack().size() > previousSize) {
            double rightParam = context.getArgumentsStack().pop();
            double leftParam = context.getArgumentsStack().pop();
            double result = context.getOperatorsStack().pop().exec(leftParam, rightParam);
            context.getArgumentsStack().push(result);
        }
    }
}
