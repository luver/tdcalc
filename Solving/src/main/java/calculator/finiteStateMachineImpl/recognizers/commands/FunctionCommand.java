package calculator.finiteStateMachineImpl.recognizers.commands;

import calculator.finiteStateMachineImpl.EvaluatingContext;
import calculator.functions.Function;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class FunctionCommand implements InterpretingCommand {
    private final Function function;

    public FunctionCommand(Function function) {
        this.function = function;
    }

    @Override
    public void execute(EvaluatingContext context) {
        context.getFunctionsStack().push(function);
        context.getFunctionPositionStack().push(context.getOperatorsStack().size());
        context.getArgumentsCountStack().push(context.getArgumentsStack().size());
    }
}
