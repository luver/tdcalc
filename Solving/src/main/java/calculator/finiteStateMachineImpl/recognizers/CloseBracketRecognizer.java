package calculator.finiteStateMachineImpl.recognizers;

import calculator.finiteStateMachineImpl.ParsingContext;
import calculator.finiteStateMachineImpl.recognizers.commands.CloseBracketCommand;
import calculator.finiteStateMachineImpl.recognizers.commands.InterpretingCommand;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class CloseBracketRecognizer implements Recognizer {
    final private char closingBracketSymbol = ')';

    @Override
    public InterpretingCommand getCommand(ParsingContext context) {
        if (parse(context)) {
            return new CloseBracketCommand();
        }
        return null;
    }

    private boolean parse(ParsingContext context) {
        String expression = context.getExpression();
        while (!context.isParsed() && expression.charAt(context.getCursor()) == ' ') {
            context.setCursor(context.getCursor() + 1);
        }
        if (!context.isParsed()) {
            if (expression.charAt(context.getCursor()) == closingBracketSymbol) {
                context.setCursor(context.getCursor() + 1);
                return true;
            }
        }
        return false;
    }
}
