package calculator.finiteStateMachineImpl.recognizers;

import calculator.finiteStateMachineImpl.ParsingContext;
import calculator.finiteStateMachineImpl.recognizers.commands.InterpretingCommand;
import calculator.finiteStateMachineImpl.recognizers.commands.OperatorCommand;
import calculator.operators.BinaryOperator;
import calculator.operators.BinaryOperatorFactoriesHolder;
import calculator.operators.BinaryOperatorProvider;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class OperatorRecognizer implements Recognizer {
    private BinaryOperatorProvider operatorProvider;
    private String alphabet = "+-/*\\'^&!@#$|<>%:;*";

    public OperatorRecognizer(BinaryOperatorFactoriesHolder holder) {
        operatorProvider = new BinaryOperatorProvider(holder);
    }

    @Override
    public InterpretingCommand getCommand(ParsingContext context) {
        BinaryOperator operator = getOperator(context);
        if (operator != null) {
            return new OperatorCommand(operator);
        } else {
            return null;
        }
    }

    private BinaryOperator getOperator(ParsingContext context) {
        String operator = "";
        while (!context.isParsed()) {
            if (alphabet.indexOf(context.getCurrentChar()) != -1) {
                operator = operator + context.getCurrentChar();
                context.incCursor();
            } else {
                break;
            }
        }
        return operatorProvider.get(operator);
    }
}
