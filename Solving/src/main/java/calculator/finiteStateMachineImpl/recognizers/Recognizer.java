package calculator.finiteStateMachineImpl.recognizers;

import calculator.finiteStateMachineImpl.ParsingContext;
import calculator.finiteStateMachineImpl.recognizers.commands.InterpretingCommand;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public interface Recognizer {
    InterpretingCommand getCommand(ParsingContext context);
}
