package calculator.finiteStateMachineImpl.recognizers;

import calculator.finiteStateMachineImpl.ParsingContext;
import calculator.finiteStateMachineImpl.recognizers.commands.InterpretingCommand;
import calculator.finiteStateMachineImpl.recognizers.commands.NumberCommand;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParsePosition;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class NumberRecognizer implements Recognizer {
    @Override
    public InterpretingCommand getCommand(ParsingContext context) {
        try {
            double number = parse(context);
            return new NumberCommand(number);
        } catch (Exception e) {
            return null;
        }
    }

    private double parse(ParsingContext context) {
        while (!context.isParsed() && context.getExpression().charAt(context.getCursor()) == ' ') {
            context.setCursor(context.getCursor() + 1);
        }
        ParsePosition parsePosition = new ParsePosition(context.getCursor());
        NumberFormat numberFormat = new DecimalFormat("0.0");
        Number number = numberFormat.parse(context.getExpression(), parsePosition);

        if (parsePosition.getErrorIndex() == -1) {
            context.setCursor(parsePosition.getIndex());
            return number.doubleValue();
        } else {
            throw new RuntimeException("Number is expected");
        }
    }
}
