package calculator.finiteStateMachineImpl.recognizers;

import calculator.finiteStateMachineImpl.ParsingContext;
import calculator.finiteStateMachineImpl.recognizers.commands.InterpretingCommand;
import calculator.finiteStateMachineImpl.recognizers.commands.OpenBracketCommand;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class OpenBracketRecognizer implements Recognizer {
    final private char openingBracketSymbol = '(';

    @Override
    public InterpretingCommand getCommand(ParsingContext context) {
        if (parse(context)) {
            return new OpenBracketCommand();
        }
        return null;
    }

    private boolean parse(ParsingContext context) {
        String expression = context.getExpression();
        while (!context.isParsed() && expression.charAt(context.getCursor()) == ' ') {
            context.setCursor(context.getCursor() + 1);
        }
        if (!context.isParsed()) {
            if (expression.charAt(context.getCursor()) == openingBracketSymbol) {
                context.setCursor(context.getCursor() + 1);
                return true;
            }
        }
        return false;
    }
}
