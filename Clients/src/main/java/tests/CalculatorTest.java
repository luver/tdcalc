package tests;

import calculator.api.Solver;
import calculator.api.SolvingException;
import calculator.finiteStateMachineImpl.FiniteStateMachineSolver;
import org.junit.Test;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */

public class CalculatorTest {
    @Test
    public void testWithSpaces() {
        Solver solver = new FiniteStateMachineSolver();
        try {
            System.out.println(solver.solve("2 + 3 - 4 * 5 / 2^6 + sum()"));
            assert 7.6875 == solver.solve("2 + 3 - 4 * 5 / 2^6 + sum()");
        } catch (SolvingException e) {
            System.out.println(e.getMessage() + " at " + e.getErrorIndex());
            assert false;
        }

    }
}
