package tests;

import calculator.operators.Priority;
import org.junit.Test;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class PriorityTest {
    @Test
    public void testPriorityComparing() {
        assert Priority.HIGH.compareTo(Priority.LOW) < 0;
        assert Priority.HIGH.compareTo(Priority.HIGH) == 0;
        assert Priority.HIGH.compareTo(Priority.HIGHEST) > 0;

    }
}
