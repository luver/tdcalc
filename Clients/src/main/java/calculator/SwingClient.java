package calculator;

import calculator.api.Solver;
import calculator.api.SolvingException;
import calculator.finiteStateMachineImpl.FiniteStateMachineSolver;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class SwingClient implements Client {
    JFrame mainFrame = new JFrame("Calculator");
    JPanel mainPanel = new JPanel();
    JTextField expression = new JTextField();
    JLabel result = new JLabel();
    JButton button = new JButton("Solve");
    Solver solver = new FiniteStateMachineSolver();

    @Override
    public void run() {
        mainFrame.setPreferredSize(new Dimension(300, 100));
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
        expression.setMaximumSize(new Dimension(500, 20));
        mainPanel.add(expression);
        mainPanel.add(result);
        mainPanel.add(button);
        mainFrame.add(mainPanel);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Double res = solver.solve(expression.getText());
                    result.setText(res.toString());
                } catch (SolvingException e1) {
                    result.setText(e1.getMessage() + " at " + e1.getErrorIndex());
                    expression.setCaretPosition(e1.getErrorIndex() - 1);
                    expression.requestFocus();
                }
            }
        });
        mainFrame.pack();
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setVisible(true);

    }
}
                      