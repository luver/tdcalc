package calculator;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class Main {
    public static void main(String[] args) {
        if (args.length > 0) {
            Client client = new ConsoleClient();
            client.run();
        } else {
            Client client = new SwingClient();
            client.run();
        }

    }
}
